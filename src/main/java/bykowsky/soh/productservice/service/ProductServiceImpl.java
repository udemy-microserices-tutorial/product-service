package bykowsky.soh.productservice.service;

import bykowsky.soh.productservice.entity.Product;
import bykowsky.soh.productservice.exception.ErrorCode;
import bykowsky.soh.productservice.exception.ProductServiceException;
import bykowsky.soh.productservice.model.ProductRequest;
import bykowsky.soh.productservice.model.ProductResponse;
import bykowsky.soh.productservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public long addProduct(ProductRequest productRequest) {
        log.info("Adding product [{}] ", productRequest);

        Product product = Product.builder()
                .productName(productRequest.getName())
                .price(productRequest.getPrice())
                .quantity(productRequest.getQuantity())
                .build();

        log.info("Created product [{}]", product);
        return productRepository.save(product).getProductId();
    }

    @Override
    public ProductResponse getProductById(Integer id) {
        log.info("Getting product by id [{}] ", id);

        Product product = productRepository.findById(id)
                .orElseThrow(() ->
                        new ProductServiceException("Product with given id not found", ErrorCode.PRODUCT_NOT_FROUND));

        log.info("Found product [{}]", product);
        return ProductResponse.builder()
                .productId(product.getProductId())
                .productName(product.getProductName())
                .price(product.getPrice())
                .quantity(product.getQuantity())
                .build();
    }

    @Override
    public void reduceQuantity(Integer id, Integer quantity) {
        log.info("Reducing quantity by {} for product with Id: {}", quantity, id);

        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ProductServiceException("Product with given id not found. Product id: " + id,
                        ErrorCode.PRODUCT_NOT_FROUND));

        if (product.getQuantity() < quantity) {
            throw new ProductServiceException("Product does not have sufficient quantity",
                    ErrorCode.INSUFFICIENT_QUANTITY);
        }

        product.setQuantity(product.getQuantity() - quantity);
        productRepository.save(product);

        log.info("Product quantity updated successfully");
    }
}
