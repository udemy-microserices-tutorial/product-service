package bykowsky.soh.productservice.service;

import bykowsky.soh.productservice.model.ProductRequest;
import bykowsky.soh.productservice.model.ProductResponse;

public interface ProductService {
    long addProduct(ProductRequest productRequest);

    ProductResponse getProductById(Integer id);

    void reduceQuantity(Integer id, Integer quantity);
}
