package bykowsky.soh.productservice.exception;

public enum ErrorCode {
    PRODUCT_NOT_FROUND,
    INSUFFICIENT_QUANTITY
}
