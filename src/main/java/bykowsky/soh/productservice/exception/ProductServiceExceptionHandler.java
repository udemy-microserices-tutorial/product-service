package bykowsky.soh.productservice.exception;

import bykowsky.soh.productservice.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class ProductServiceExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ProductServiceException.class)
    public ResponseEntity<ErrorResponse> handleProductServiceException(ProductServiceException ex) {

        ErrorResponse errorResponse = ErrorResponse.builder()
                .errorMessage(ex.getMessage())
                .errorCode(ex.getErrorCode().toString())
                .build();

        HttpStatus httpStatus = HttpStatus.NOT_FOUND;

        if (ErrorCode.INSUFFICIENT_QUANTITY == ex.getErrorCode()) {
            httpStatus = HttpStatus.NOT_ACCEPTABLE;
        }

        log.debug("Handling ProductServiceException: {}", ex);
        return new ResponseEntity<>(errorResponse, httpStatus);
    }
}
