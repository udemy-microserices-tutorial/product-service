package bykowsky.soh.productservice.exception;

import lombok.Data;

@Data
public class ProductServiceException extends RuntimeException {
    private ErrorCode errorCode;

    public ProductServiceException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
}
